<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class stokController extends Controller{
    
    public function tambahBarang(Request $req){
        //raw query -> mysqli_query("insert into barang value()")
        DB::table("barang")->insert([
            "nama_barang"=>$req->nama_barang,
            "jenis_barang"=>$req->jenis_barang,
            "stok_barang"=>$req->stok_barang,
            "created_at"=>now()
        ]);  

        return redirect('/');
    }
        
    public function lihatBarang(){
        $lihat = DB::table("barang")->get();
        return view('barang')->with('lihat',$lihat);
    }

    public function hapusBarang($id){
        DB::table("barang")->where('id','=',$id)->delete();

        return redirect('lihat-barang');
    }
}
