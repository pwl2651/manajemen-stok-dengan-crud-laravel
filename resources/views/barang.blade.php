<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Website Saya</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" integrity="sha384-GLhlTQ8iN17P6L+qfB9sZd/3uJlz9SM+97usDSkqGgFAW8aX8eOrCvZLcOIdUmeT" crossorigin="anonymous">

    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&family=Poppins:wght@400;700&display=swap">

    <style>
        body {
            font-family: 'Montserrat', sans-serif;
        }

        header {
            font-family: 'Poppins', sans-serif;
            background-color: #333;
            color: #83EEFF;
            padding: 0.01vw;
            text-align: center;
        }

        nav {
            background-color: #eee;
            padding: 0.1vw;
        }

        main {
            padding: 0.2vw;
        }

        footer {
            background-color: #333;
            color: #fff;
            padding: 1vw;
            text-align: center;
        }
        .text-end {
            text-align: right;
        }

        /* Add your additional styles here */

    </style>
</head>
<body>
@extends('layouts.app')
@section('content')
    <main>
        <div class="container mt-4">
            <h2>Main Content</h2>
            <hr style="border: 1px solid #555;">
            <!-- Responsive Table with DataTable -->
            <div class="text-end mb-3">
                <a href="/form-barang">
                <button class="btn btn-primary" id="addButton" >Tambah Barang</button>
                </a>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered text-center" id="dataTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Barang</th>
                            <th>Jenis Barang</th>
                            <th>Stok Barang</th>
                            <th>Tanggal Dibuat</th>
                            <th>Tanggal Update</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody id="tableBody">
                        @forelse ($lihat as $item)
                        <tr id="isi">
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->nama_barang }}</td>
                            <td>{{ $item->jenis_barang }}</td>
                            <td>{{ $item->stok_barang }}</td>
                            <td>{{ $item->created_at }}</td>
                            <td>{{ $item->updated_at }}</td>
                            <td class="text-center align-middle">
                                <a class="fas fa-trash center" href="hapus-barang/{{$item->id}}">
                                    <button class="btn btn-danger">
                                        HAPUS
                                    </button>
                                </a>
                            </td>
                        </tr>
                        @empty
                        <tr id="noDataMessage">
                            <td colspan="7" class="text-center">Tidak ada data untuk ditampilkan.</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
                {{-- <p id="noDataMessage" class="text-center">Tidak ada data untuk ditampilkan.</p> --}}
            </div>
        </div>
    </main>

    <!-- Footer -->
    <footer>
        <p>&copy; 2024 Web Design 3.0. All rights reserved.</p>
        <p>About Us | Contact | Privacy Policy</p>
    </footer>
@endsection
    <!-- Bootstrap JS and DataTable JS -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.6/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.6/js/dataTables.bootstrap5.min.js"></script>

</body>
</html>
