<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Dashboard</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
    <style>
        body {
            font-family: 'Montserrat', sans-serif;
        }
        .sidebar {
            width: 250px;
            position: fixed;
            top: 0;
            left: -250px;
            height: 100%;
            background-color: #333;
            color: #fff;
            transition: left 0.3s;
        }
        .sidebar.show {
            left: 0;
        }
        .sidebar.hide {
            left: -250px;
        }
        .content {
            margin-left: 250px;
            padding: 20px;
        }
        .footer {
            text-align: center;
            background-color: #333;
            color: #fff;
            padding: 10px;
        }
    </style>
</head>
<body>
    <div class="sidebar">
        <h2>Dashboard</h2>
        <ul>
            <li><a href="#">Dashboard</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Messages</a></li>
            <li><a href="#">Settings</a></li>
        </ul>
    </div>
    <div class="content">
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container">
                    <a class="navbar-brand" href="#">Web Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">About</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Contact</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="main-body">
            <!-- Konten utama dashboard di sini -->
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis hendrerit sapien. 
            Sed vestibulum eget tortor in eleifend. Sed non suscipit libero. Aliquam erat volutpat. 
            Nulla malesuada metus ac tristique tempus. Sed sit amet arcu quam. 
            Suspendisse ullamcorper, augue a cursus volutpat, justo eros convallis urna, id fringilla purus urna in libero. 
            Etiam et cursus sapien. Proin at orci metus. Sed non feugiat dui. Nulla facilisi. 
            Nunc ut aliquet metus. Nulla eu nunc id tortor suscipit tempus. 
            Curabitur scelerisque lorem eu ligula interdum, eu consectetur purus malesuada.
            </p><p>
            Sed pulvinar dui ut metus euismod, id vehicula dolor egestas. Fusce ut arcu ut velit efficitur tincidunt. 
            Vivamus non tellus in lacus volutpat vulputate. Fusce fringilla euismod ligula, a elementum elit tincidunt at. 
            Nunc venenatis sagittis justo, a venenatis quam lacinia in. Sed non semper urna. 
            Curabitur et aliquet nulla. Sed ac libero at tortor vestibulum laoreet a non odio. 
            Curabitur sit amet libero eget nunc sagittis tincidunt. Suspendisse potenti. 
            Proin ac viverra tellus, a tristique ligula. Cras bibendum in nunc a hendrerit. 
            Sed auctor vel ex a fermentum. Vivamus viverra, purus a fermentum venenatis, ex mi aliquam purus, 
            ac pharetra ipsum ligula sit amet arcu. Vivamus euismod, orci nec lacinia fermentum, 
            lectus ipsum bibendum mi, vel facilisis purus sapien vel purus.
        </p>
        </div>
    </div>
    <div class="footer">
        <p>&copy; 2023 Web Dashboard. All rights reserved.</p>
    </div>

    <!-- Tambahkan JavaScript untuk mengatur perilaku sidebar yang dapat di-hide -->
    <script>
        document.querySelector('.sidebar-toggle').addEventListener('click', function() {
            document.querySelector('.sidebar').classList.toggle('show');
            document.querySelector('.sidebar').classList.toggle('hide');
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.min.js"></script>
</body>
</html>
