<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Web Design 3.0</title>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Montserrat Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap">

    <!-- Poppins Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap">

    <!-- Font Awesome Icons (Free and Fair Use) -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <style>
        body {
            font-family: 'Montserrat', sans-serif;
        }

        header {
            font-family: 'Poppins', sans-serif;
            background-color: #333;
            color: #83EEFF;
            padding: 0.01vw;
            text-align: center;
        }

        nav {
            background-color: #eee;
            padding: 0.1vw;
        }

        main {
            padding: 0.2vw;
        }

        footer {
            background-color: #333;
            color: #fff;
            padding: 1vw;
            text-align: center;
        }
    </style>
</head>

<body>
@extends('layouts.app')
@section('content')
<main>
    <div class="container">
        <h2>Barang Stocker</h2>
        <hr style="border: 1px solid #555;">
        <!-- Content input for product name and type -->
        <form method="post" action="/tambah-form-barang">
            @csrf
            <div class="mb-3">
                <label for="namaBarang" class="form-label">Nama Produk</label>
                <input type="text" class="form-control" id="namaBarang" name="nama_barang" placeholder="Masukan Nama Barang">
            </div>
            <div class="mb-3">
                <label for="jenisBarang" class="form-label">Jenis Barang</label>
                <input type="text" class="form-control" id="jenisBarang" name="jenis_barang" placeholder="Masukan Jenis Barang">
            </div>
            <div class="mb-3">
                <label for="stokBarang" class="form-label">Jumlah Barang</label>
                <input type="number" class="form-control" id="stokBarang" name="stok_barang" placeholder="Masukan Jumlahnya">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
        <br>
    </div>
</main>

<footer>
    <p>&copy; 2024 Web Design 3.0. All rights reserved.</p>
    <p>About Us | Contact | Privacy Policy</p>
</footer>
@endsection

<!-- Bootstrap JS (Optional) -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>
