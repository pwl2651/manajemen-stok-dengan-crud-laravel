<!DOCTYPE html>
<html>
<head>
    <title>Dashboard Sederhana</title>
    <!-- Tambahkan link ke Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

<!-- Header -->
<header class="bg-primary text-white text-center py-4">
    <h1>Dashboard Sederhana</h1>
</header>

<!-- Navigasi -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Beranda</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">Menu 1</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Menu 2</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Menu 3</a>
            </li>
        </ul>
    </div>
</nav>

<!-- Konten Utama -->
<div class="container mt-4">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-success">
                Selamat datang di dashboard sederhana ini!
            </div>
            <div class="alert alert-success">
                Nama Anda : {{$request->nama }}
            </div>
            <div class="alert alert-success">
                Jabatan Anda : {{$request->jabatan }}
            </div>
            <div class="alert alert-success">
                Gaji Anda : {{$request->gaji }}
            </div>
            <div class="alert alert-success">
                @if( $request->gaji > 5000000 )
                    {{ $pph = 0.1 }}
                @else
                    {{ $pph = 0 }}
                @endif
                Pajak Anda : {{ $pajak = $request->gaji * $pph }}
            </div>
            <div class="alert alert-success">
                Total gaji Anda : {{$request->gaji - $pajak }}
            </div>
        </div>
    </div>
</div>

<!-- Tambahkan link ke Bootstrap JS dan jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>