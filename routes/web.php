<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\stokController;
use App\Http\Controllers\maincore;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// V-V-V OLD ROUTES. BETTER NOT TO USE IT V-V-V
// V-V-V OLD ROUTES. BETTER NOT TO USE IT V-V-V

Route::get('bladebelajar', function(Request $request){
    return view ('belajar')->with('request', $request);
});

Route::get('bladecoba',function(){
    return view ('ujicoba.coba');
});

Route::get('slipgaji', function(Request $req){
    return view ('slipgaji')->with('request', $req);
});

Route::get('lihatcore', [maincore::class, /*nama function =>*/'lihat']);

// ^^^ OLD ROUTES. BETTER NOT TO USE IT ^^^
// ^^^ OLD ROUTES. BETTER NOT TO USE IT ^^^

Route::get('/',[stokController::class, 'lihatBarang'])->middleware("auth");

Route::get('form-barang',function(){
    return view("formBarang");
});
Route::post('tambah-form-barang',[stokController::class, 'tambahBarang']);
Route::get('hapus-barang/{id}',[stokController::class, 'hapusBarang']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();